﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using System;
using System.Security.Cryptography;

namespace Benchmark
{
    class Program
    {
        static void Main(string[] args)
        {
            //DEMO BENCHMARK
            //--- NOTE: MUST RUN IN RELEASE MODE
            //--- NOTE: results are placed in: \bin\Release\netcoreapp2.1\BenchmarkDotNet.Artifacts\results

            var summary = BenchmarkRunner.Run<Md5VsSha256>();
            Console.WriteLine(summary.Title);
        }
    }

    public class Md5VsSha256
    {
        private const int N = 10000;
        private readonly byte[] data;

        private readonly SHA256 sha256 = SHA256.Create();
        private readonly MD5 md5 = MD5.Create();

        public Md5VsSha256()
        {
            data = new byte[N];
            new Random(42).NextBytes(data);
        }

        [Benchmark]
        public byte[] Sha256() => sha256.ComputeHash(data);

        [Benchmark]
        public byte[] Md5() => md5.ComputeHash(data);
    }
}
