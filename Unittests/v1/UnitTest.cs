﻿// <copyright file="UnitTest.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Unittests
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using ITUtil.Microservice.V1;
    using ITUtil.SmsNotification.V1.DTO;
    using NUnit.Framework;

    /// <summary>
    /// UnitTest
    /// </summary>
    [TestFixture]
    public class UnitTest
    {
        /// <summary>
        /// SaveSMSSettings
        /// </summary>
        [Test]
        public void SaveSMSSettings()
        {
            string gatewayvalue = "TEST";
            Settings.SaveSettings(new Settings() { smsGatewayId = gatewayvalue });
            var loaded = Settings.LoadSettings();
            Assert.AreEqual(gatewayvalue, loaded.smsGatewayId, "Loaded settings should be the same as saved settings");
        }

        /// <summary>
        /// SendSmsWithOutFrom
        /// </summary>
        [Test]
        public void SendSmsWithOutFrom()
        {
            var receiver = "42232081";
            var smsmsg = new SmsNotification() { from = " ", receivers = new List<string> { receiver }, message = "hej med dig" };
            var settings = new Settings() { smsGatewayId = "FBC1E05A-4D54-4D0A-974A-1DA3192CFAB3" };
            var ex = Assert.Throws<ValidationException>(() => SmsFunction.SendSms(smsmsg, settings));
            Assert.AreEqual("You have not add a name or number on the person there is sending this sms", ex.Message);
        }

        /// <summary>
        /// SendSmsWithOutReceivers
        /// </summary>
        [Test]
        public void SendSmsWithOutReceivers()
        {
            var receiver = " ";
            var smsmsg = new SmsNotification() { from = "42232081", receivers = new List<string> { receiver }, message = "hej med dig" };
            var settings = new Settings() { smsGatewayId = "FBC1E05A-4D54-4D0A-974A-1DA3192CFAB3" };
            var ex = Assert.Throws<ValidationException>(() => SmsFunction.SendSms(smsmsg, settings));
            Assert.AreEqual("You have not add a phone number on the person there going to receive this sms", ex.Message);
        }

        /// <summary>
        /// SendSmsWithOutMessage
        /// </summary>
        [Test]
        public void SendSmsWithOutMessage()
        {
            var receiver = "42232081";
            var smsmsg = new SmsNotification() { from = "42232081", receivers = new List<string> { receiver }, message = " " };
            var settings = new Settings() { smsGatewayId = "FBC1E05A-4D54-4D0A-974A-1DA3192CFAB3" };
            var ex = Assert.Throws<ValidationException>(() => SmsFunction.SendSms(smsmsg, settings));
            Assert.AreEqual("you'r message in emty in this sms", ex.Message);
        }

        /// <summary>
        /// SendSmsWithOutSmsGatewayId
        /// </summary>
        [Test]
        public void SendSmsWithOutSmsGatewayId()
        {
            var receiver = "42232081";
            var smsmsg = new SmsNotification() { from = "42232081", receivers = new List<string> { receiver }, message = "hej med dig" };
            var settings = new Settings() { smsGatewayId = " " };
            var ex = Assert.Throws<ValidationException>(() => SmsFunction.SendSms(smsmsg, settings));
            Assert.AreEqual("you SMSGatewayId is emty, you need it to send sms", ex.Message);
        }

        /// <summary>
        /// SendSmsWithCharacteInReceiver
        /// </summary>
        [Test]
        public void SendSmsWithCharacteInReceiver()
        {
            var receiver = "42232081f ";
            var smsmsg = new SmsNotification() { from = "42232081", receivers = new List<string> { receiver }, message = "hej med dig" };
            var settings = new Settings() { smsGatewayId = "FBC1E05A-4D54-4D0A-974A-1DA3192CFAB3" };
            var ex = Assert.Throws<ValidationException>(() => SmsFunction.SendSms(smsmsg, settings));
            Assert.AreEqual("The number you have add for receiver is not a number", ex.Message);
        }
    }
}
