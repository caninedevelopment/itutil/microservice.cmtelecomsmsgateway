﻿// <copyright file="CmTelecomErrorCodes.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Notification.V1
{
    /// <summary>
    /// CmTelecomErrorCodes
    /// </summary>
    public static class CmTelecomErrorCodes
    {
        /// <summary>
        /// ErrorCode
        /// </summary>
        public enum ErrorCode
        {
            /// <summary>
            /// ok.
            /// </summary>
            Ok = 0,

            /// <summary>
            /// Unknown_error.
            /// </summary>
            Unknown_error = 999,

            /// <summary>
            /// Authentiction_of_the_request_failed.
            /// </summary>
            Authentiction_of_the_request_failed = 101,

            /// <summary>
            /// The_account_using_this_authentiction_has_insuffcient_balance.
            /// </summary>
            The_account_using_this_authentiction_has_insuffcient_balance = 102,

            /// <summary>
            /// The_product_token_is_incorrect.
            /// </summary>
            The_product_token_is_incorrect = 103,

            /// <summary>
            /// This_request_has_one_or_more_errors_in_its_messages_Some_or_all_messages_have_not_been_sent_See_MSGs_for_details.
            /// </summary>
            This_request_has_one_or_more_errors_in_its_messages_Some_or_all_messages_have_not_been_sent_See_MSGs_for_details = 201,

            /// <summary>
            /// This_request_is_malformed_please_confirm_the_JSON_and_that_the_correct_data_types_are_used.
            /// </summary>
            This_request_is_malformed_please_confirm_the_JSON_and_that_the_correct_data_types_are_used = 202,

            /// <summary>
            /// The_requests_MSG_array_is_incorrect.
            /// </summary>
            The_requests_MSG_array_is_incorrect = 203,

            /// <summary>
            /// This_MSG_has_an_invalid_From_field_per_msg.
            /// </summary>
            This_MSG_has_an_invalid_From_field_per_msg = 301,

            /// <summary>
            /// This_MSG_has_an_invalid_To_fieldper_msg.
            /// </summary>
            This_MSG_has_an_invalid_To_fieldper_msg = 302,

            /// <summary>
            /// This_MSG_has_an_invalid_Phone_Number_in_the_To_fieldper_msg.
            /// </summary>
            This_MSG_has_an_invalid_Phone_Number_in_the_To_fieldper_msg = 303,

            /// <summary>
            /// This_MSG_has_an_invalid_Body_fieldper_msg.
            /// </summary>
            This_MSG_has_an_invalid_Body_fieldper_msg = 304,

            /// <summary>
            /// This_MSG_has_an_invalid_fieldPlease_confirm_with_the_documentation_per_msg.
            /// </summary>
            This_MSG_has_an_invalid_fieldPlease_confirm_with_the_documentation_per_msg = 305,

            /// <summary>
            /// Message_has_been_spam_filtered.
            /// </summary>
            Message_has_been_spam_filtered = 401,

            /// <summary>
            /// Message_has_been_blacklisted.
            /// </summary>
            Message_has_been_blacklisted = 402,

            /// <summary>
            /// Message_has_been_rejected.
            /// </summary>
            Message_has_been_rejected = 403,

            /// <summary>
            /// An_internal_error_has_occurredhas_occurred.
            /// </summary>
            An_internal_error_has_occurredhas_occurred = 500,
        }
    }
}
