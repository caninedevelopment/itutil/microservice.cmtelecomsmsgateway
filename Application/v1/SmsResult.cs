﻿// <copyright file="SmsResult.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Notification.V1
{
    using static Monosoft.Service.Notification.V1.CmTelecom;

    /// <summary>
    /// SmsResult
    /// </summary>
    public static class SmsResult
    {
        /// <summary>
        /// SmsFunctionResult
        /// </summary>
        public class SmsFunctionResult
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="SmsFunctionResult"/> class.
            /// </summary>
            /// <param name="statustext">statustext string</param>
            /// <param name="response">response string</param>
            public SmsFunctionResult(string statustext, string response)
            {
                this.StatusText = statustext;
                this.ResponseText = Newtonsoft.Json.JsonConvert.DeserializeObject<CMTelecomResultObj>(response);
            }

            /// <summary>
            /// Gets or sets statusText
            /// </summary>
            public string StatusText { get; set; }

            /// <summary>
            /// Gets or sets responseText
            /// </summary>
            public CMTelecomResultObj ResponseText { get; set; }
        }
    }
}
