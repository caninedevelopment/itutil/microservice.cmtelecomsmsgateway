﻿// <copyright file="SmsFunction.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Microservice.V1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Net.Http;
    using System.Text;
    using System.Text.RegularExpressions;
    using static Monosoft.Service.Notification.V1.SmsResult;

    /// <summary>
    /// SmsFunction
    /// </summary>
    public static class SmsFunction
    {
        /// <summary>
        /// SendSms
        /// </summary>
        /// <param name="smsmsg">SmsNotification</param>
        /// <param name="settings">Settings</param>
        /// <returns>SmsFunctionResult</returns>
        public static SmsFunctionResult SendSms(ITUtil.SmsNotification.V1.DTO.SmsNotification smsmsg, ITUtil.SmsNotification.V1.DTO.Settings settings)
        {
            if (smsmsg == null)
            {
                throw new ArgumentNullException(nameof(smsmsg));
            }

            if (settings == null)
            {
                throw new ArgumentNullException(nameof(settings));
            }

            // if you change any of the ValidationException msg rember to change them in UnitTest too
            List<string> receivers = new List<string>();
            foreach (var recev in smsmsg.receivers)
            {
                var listReceiver = recev.Replace(" ", string.Empty, StringComparison.OrdinalIgnoreCase);
                if (listReceiver.Length > 0)
                {
                    if (listReceiver.StartsWith("+", StringComparison.OrdinalIgnoreCase))
                    {
                        listReceiver = listReceiver.Replace("+", "00", StringComparison.OrdinalIgnoreCase);
                    }

                    var regex = @"^[0-9]*$";
                    var match = Regex.Match(listReceiver, regex, RegexOptions.IgnoreCase);
                    if (!match.Success)
                    {
                        throw new ValidationException("The number you have add for receiver is not a number");
                    }

                    receivers.Add(listReceiver);
                }
            }

            if (smsmsg.from.Contains("+", StringComparison.OrdinalIgnoreCase))
            {
                smsmsg.from = smsmsg.from.Replace("+", "00", StringComparison.OrdinalIgnoreCase);
            }

            if (receivers.Count == 0)
            {
                throw new ValidationException("You have not add a phone number on the person there going to receive this sms");
            }

            if (smsmsg.from.Replace(" ", string.Empty, StringComparison.OrdinalIgnoreCase).Length == 0)
            {
                throw new ValidationException("You have not add a name or number on the person there is sending this sms");
            }

            if (smsmsg.message.Replace(" ", string.Empty, StringComparison.OrdinalIgnoreCase).Length == 0)
            {
                throw new ValidationException("you'r message in emty in this sms");
            }

            if (settings.smsGatewayId.Replace(" ", string.Empty, StringComparison.OrdinalIgnoreCase).Length == 0)
            {
                throw new ValidationException("you SMSGatewayId is emty, you need it to send sms");
            }

            if (settings.smsGatewayId.Length == 0)
            {
                throw new ValidationException("Du har sat en SMSGatewayId");
            }

            var receiver = string.Empty;
            for (int i = 0; i < receivers.Count; i++)
            {
                if (i != 0)
                {
                    receiver += ",";
                }

                receiver += "{'number': '" + receivers[i] + "'}";
            }

            using (var client = new HttpClient())
            {
                var json = @"{
                            'messages': {
                                    'authentication': {
                                        'producttoken': '" + settings.smsGatewayId + @"' 
                                    },
                                    'msg':[
                                            {
                                             'allowedChannels':['SMS'],
                                             'from':'" + smsmsg.from + @"',
                                             'to':[" + receiver + @"],
                                             'body': {
                                                        'content':'" + smsmsg.message + @"
                                                  '}
                                             }
                                           ]
                                    }
                                }";

                var smsResponse = string.Empty;
                using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    var url = "https://gw.cmtelecom.com/v1.0/message";
                    var responseContent = client.PostAsync(url, stringContent).Result.Content.ReadAsStringAsync().Result;
                    smsResponse = responseContent;
                }

                return new SmsFunctionResult("SMS sent", smsResponse);
            }
        }
    }
}
