﻿// <copyright file="SmsNotification.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.SmsNotification.V1.DTO
{
    using System.Collections.Generic;

    /// <summary>
    /// SmsNotification.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
    public class SmsNotification
    {
        /// <summary>
        /// Gets or sets from
        /// </summary>
        public string from { get; set; }

        /// <summary>
        /// Gets or sets receivers
        /// </summary>
        public List<string> receivers { get; set; }

        /// <summary>
        /// Gets or sets message
        /// </summary>
        public string message { get; set; }
    }
}
