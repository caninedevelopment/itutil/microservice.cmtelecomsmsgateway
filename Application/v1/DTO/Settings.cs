﻿// <copyright file="Settings.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.SmsNotification.V1.DTO
{
    /// <summary>
    /// Settings.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
    public class Settings
    {
        /// <summary>
        /// Gets or sets smsGatewayId
        /// </summary>
        public string smsGatewayId { get; set; }

        private static string settingsfile = "notification.json";

        /// <summary>
        /// Gets or sets the current/active diagnostics settings.
        /// </summary>
        /// <returns>
        /// retun settings.
        /// </returns>
        public static DTO.Settings LoadSettings()
        {
            if (System.IO.File.Exists(settingsfile))
            {
                var json = System.IO.File.ReadAllText(settingsfile);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<DTO.Settings>(json);
            }
            else
            {
                return new DTO.Settings();
            }
        }

        /// <summary>
        /// SaveSettings
        /// </summary>
        /// <param name="value">DTO.Settings value</param>
        public static void SaveSettings(DTO.Settings value)
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(value);
            System.IO.File.WriteAllText(settingsfile, json);
        }
    }
}
