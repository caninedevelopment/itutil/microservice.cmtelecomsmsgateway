﻿// <copyright file="CmTelecom.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace Monosoft.Service.Notification.V1
{
    using System.Collections.Generic;

    /// <summary>
    /// CmTelecom
    /// </summary>
    public static class CmTelecom
    {
        /// <summary>
        /// CMTelecomResultObj
        /// </summary>
        public class CMTelecomResultObj
        {
            /// <summary>
            /// Gets or sets Details
            /// </summary>
            public string Details { get; set; }

            /// <summary>
            /// Gets or sets ErrorCode
            /// </summary>
            public int ErrorCode { get; set; }

            /// <summary>
            /// Gets or sets Messages
            /// </summary>
            public List<string> Messages { get; set; }
        }
    }
}
