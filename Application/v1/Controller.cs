﻿// <copyright file="Controller.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>

namespace ITUtil.Microservice.V1
{
    using System;
    using System.Collections.Generic;
    using ITUtil.Common.DTO;
    using ITUtil.Common.Utils;
    using ITUtil.Common.Utils.Bootstrapper;
    using static ITUtil.Common.Utils.Bootstrapper.OperationDescription;

    /// <summary>
    /// Controller
    /// </summary>
    public class Controller : IMicroservice
    {
        /// <inheritdoc/>
        public string ServiceName
        {
            get { return "cmtelecomSmsGate"; }
        }

        /// <inheritdoc/>
        public ProgramVersion ProgramVersion
        {
            get
            {
                return new ProgramVersion("1.0.0.0");
            }
        }

        private static readonly MetaDataDefinition SendSmsClaim = new MetaDataDefinition("Monosoft.Service.SmsNotification", "sendsms", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("en", "User is allowed to send sms"));
        private static readonly MetaDataDefinition AdminClaim = new MetaDataDefinition("Monosoft.Service.SmsNotification", "administrator", MetaDataDefinition.DataContextEnum.organisationClaims, new LocalizedString("en", "User is allowed to setup sms settings or see sms settings"));

        /// <summary>
        /// Gets OperationDescription
        /// </summary>
        public List<OperationDescription> OperationDescription
        {
            get
            {
                return new List<OperationDescription>()
                {
                   new OperationDescription(
                       "SetSettings",
                       "Update the CmTelecom account settings",
                       typeof(ITUtil.SmsNotification.V1.DTO.Settings),
                       null,
                       new MetaDataDefinitions(new MetaDataDefinition[] { AdminClaim }),
                       ExecuteOperationSetSettings),
                   new OperationDescription(
                       "GetSettings",
                       "Get the CmTelecom account settings",
                       null,
                       typeof(ITUtil.SmsNotification.V1.DTO.Settings),
                       new MetaDataDefinitions(new MetaDataDefinition[] { AdminClaim }),
                       ExecuteOperationGetSettings),
                   new OperationDescription(
                       "sms",
                       "Send a sms",
                       typeof(ITUtil.SmsNotification.V1.DTO.SmsNotification),
                       typeof(bool),
                       new MetaDataDefinitions(new MetaDataDefinition[] { SendSmsClaim }),
                       ExecuteOperationSMS),
                };
            }
        }

        /// <summary>
        /// ExecuteOperationSetSettings
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationSetSettings(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            var newsettings = MessageDataHelper.FromMessageData<ITUtil.SmsNotification.V1.DTO.Settings>(wrapper.messageData);
            SmsNotification.V1.DTO.Settings.SaveSettings(newsettings);
            return ReturnMessageWrapper.CreateResult(true, wrapper, null, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationGetSettings
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationGetSettings(MessageWrapper wrapper)
        {
            ITUtil.SmsNotification.V1.DTO.Settings settings = SmsNotification.V1.DTO.Settings.LoadSettings();
            return ReturnMessageWrapper.CreateResult(true, wrapper, settings, LocalizedString.OK);
        }

        /// <summary>
        /// ExecuteOperationSMS
        /// </summary>
        /// <param name="wrapper">MessageWrapper</param>
        /// <returns>ReturnMessageWrapper</returns>
        public static ReturnMessageWrapper ExecuteOperationSMS(MessageWrapper wrapper)
        {
            if (wrapper == null)
            {
                throw new ArgumentNullException(nameof(wrapper));
            }

            ITUtil.SmsNotification.V1.DTO.Settings settings = SmsNotification.V1.DTO.Settings.LoadSettings();
            var smsmsg = MessageDataHelper.FromMessageData<ITUtil.SmsNotification.V1.DTO.SmsNotification>(wrapper.messageData);

            var sms = SmsFunction.SendSms(smsmsg, settings);
            var res = sms.ResponseText.ErrorCode == 0; // TODO: returner det faktisk svar istedet for bool...

            return ReturnMessageWrapper.CreateResult(true, wrapper, res, LocalizedString.OK);
        }
    }
}
